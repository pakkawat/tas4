import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
import VueLazyload from "vue-lazyload";

Vue.use(VueRouter);
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: "../assets/noimage.png",
  loading: "../assets/loading.gif",
  attempt: 1
});

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */
var config = {
  apiKey: "AIzaSyAO6XZYR9cTvXCI-0orTpwlYm3Q2ys-FlY",
  authDomain: "winnerenglish-5f8d3.firebaseapp.com",
  databaseURL: "https://winnerenglish-5f8d3.firebaseio.com",
  projectId: "winnerenglish-5f8d3",
  storageBucket: "winnerenglish-5f8d3.appspot.com",
  messagingSenderId: "40516029824"
};

firebase.initializeApp(config);
export const db = firebase.firestore();
export const storage = firebase.storage().ref();

db.settings({
  timestampsInSnapshots: true
});

Vue.mixin({
  data() {
    return {
      appVersion: "1.3.11",
      numberIndexOf: [],
      imagepath: "https://api.winner-english.com/data",
      vdopath: "https://storage.googleapis.com/winnerenglish/upload"
    };
  },
  methods: {
    setRunNumber(start, end) {
      for (let i = start; i <= end; i++) {
        var dataValue = {
          label: i.toString(),
          value: i
        };
        this.numberIndexOf.push(dataValue);
      }
    },
    notifyRed(messages) {
      this.$q.notify({
        color: "negative",
        position: "top",
        icon: "error",
        message: messages,
        timeout: 800
      });
    },
    notifyGreen(messages) {
      this.$q.notify({
        color: "secondary",
        position: "top",
        icon: "done",
        message: messages,
        timeout: 800
      });
    },
    loadingShow(messages) {
      this.$q.loading.show({
        message: messages,
        delay: 400 // ms
      });
    },
    loadingHide() {
      setTimeout(() => {
        this.$q.loading.hide();
      }, 700);
    },
    getUnique(arr, comp) {
      const unique = arr
        .map(e => e[comp])

        // store the keys of the unique objects
        .map((e, i, final) => final.indexOf(e) === i && i)

        // eliminate the dead keys & store unique objects
        .filter(e => arr[e])
        .map(e => arr[e]);

      return unique;
    }
  }
});
export default function ( /* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({
      x: 0,
      y: 0
    }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  });

  return Router;
}
