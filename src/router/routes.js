const routes = [
  {
    path: "/",
    component: () => import("pages/Login.vue"),
    name: "login"
  },
  {
    path: "/welcome",
    name: "welcomeback",
    component: () => import("pages/Welcomeback.vue")
  },
  // Studysheet/print/vocabulary : เอกสารประกอบการเรียน/vocabulary (เนื้อหา)
  {
    path: "/studysheet/print/vocabularycontent/:level/:unit/",
    component: () => import("pages/print/Vocabularycontent.vue"),
    name: "vocabularycontent"
  },
  // Studysheet/print/vocabulary : เอกสารประกอบการเรียน/vocabulary (แบบฝึกหัด)
  {
    path: "/studysheet/print/vocabularypractice/:level/:unit/",
    component: () => import("pages/print/Vocabularypractice.vue"),
    name: "vocabularypractice"
  },
  // Studysheet/print/vocabulary : เอกสารประกอบการเรียน/vocabulary (เฉลย)
  {
    path: "/studysheet/print/vocabularyanswer/:level/:unit/",
    component: () => import("pages/print/Vocabularyanswer.vue"),
    name: "vocabularyanswer"
  },
  // Studysheet/print/grammar : เอกสารประกอบการเรียน/grammar (เนื้อหา)
  {
    path: "/studysheet/print/grammarcontent/:level/:unit/",
    component: () => import("pages/print/Grammarcontent.vue"),
    name: "grammarcontent"
  },
  // Studysheet/print/grammar : เอกสารประกอบการเรียน/grammar (แบบฝึกหัด) และ เอกสารประกอบการเรียน/grammar (เฉลย)
  {
    path:
      "/studysheet/print/grammarpractice/:level/:unit/:key/:practice/:type/:index",
    component: () => import("pages/print/Grammarpractice.vue"),
    name: "grammarpractice"
  },
  // Studysheet/print/reading : เอกสารประกอบการเรียน/reading (แบบฝึกหัด)
  {
    path: "/studysheet/print/readingcontent/:level/:unit/",
    component: () => import("pages/print/Readingcontent.vue"),
    name: "readingcontent"
  },
  // Exam : ข้อสอบ
  {
    path: "/genexam/print/",
    component: () => import("pages/print/Exam.vue"),
    name: "examprint"
  },
  // Examanswer : เฉลยข้อสอบ
  {
    path: "/genexamanswer/print/",
    component: () => import("pages/print/Examanswer.vue"),
    name: "examanswerprint"
  },
  // Teaching/flashcard : การสอน/คำศัพท์ (vocab)
  {
    path: "/teaching/flashcard/:level/:unit",
    component: () => import("pages/teaching/Flashcard.vue"),
    name: "teachingflashcard"
  },
  // Teaching/grammar : การสอน/grammar (vocab)
  {
    path: "/teaching/grammarcontent/:level/:unit",
    component: () => import("pages/teaching/Grammarcontent.vue"),
    name: "teachinggrammarcontent"
  },
  // Teaching/Reading : การสอน/Reading (vocab)
  {
    path: "/teaching/reading/:level/:unit",
    component: () => import("pages/teaching/Reading.vue"),
    name: "teachingreadingcontent"
  },
  // Teaching/phonics : การสอน/Listening & Speaking (Phonics)
  {
    path: "/teaching/phonics/:level/:unit",
    component: () => import("pages/teaching/Phoniccontent.vue"),
    name: "phonicscontent"
  },
  // Teaching/Tips : การสอน/Listening & Speaking (Phonics)
  {
    path: "/teaching/tips/:level/:unit",
    component: () => import("pages/teaching/Tipscontent.vue"),
    name: "tipscontent"
  },
  // Teaching/Conversation : การสอน/Listening & Speaking (Conversation)
  {
    path: "/teaching/Conversation/:level/:unit",
    component: () => import("pages/teaching/Conversationcontent.vue"),
    name: "Conversationcontent"
  },
  // Teaching/dictation : การสอน/dictation (vocab)
  {
    path: "/teaching/dictation/:level/:unit",
    component: () => import("pages/teaching/Dictation.vue"),
    name: "teachingdictationcontent"
  },
  // Student List : รายชื่อนักเรียน
  {
    path: "/studentlist/print/:academicyear/:class/:school",
    component: () => import("pages/print/Studentlist.vue"),
    name: "stuentlistprint"
  },
  // Score Print : ปริ้นคะแนนเก็บ
  {
    path: "/score/print/",
    component: () => import("pages/print/Score.vue"),
    name: "scoreprint",
    props: true
  },
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      // Main Menu : เมนู
      {
        path: "mainmenu",
        component: () => import("pages/Mainmenu.vue"),
        name: "mainmenu"
      },
      // Study Plan : แผนการสอน
      {
        path: "studyplan",
        component: () => import("pages/Studyplan.vue"),
        name: "studyplan"
      },
      // Study Sheet : เอกสารประกอบการเรียน
      {
        path: "studysheet",
        component: () => import("pages/Studysheet.vue"),
        name: "studysheet"
      },
      // Genexammenu : ข้อสอบ
      {
        path: "genexam",
        component: () => import("pages/Genexammenu.vue"),
        name: "genexam"
      },
      // Student List : รายชื่อนักเรียน
      {
        path: "studentlist",
        component: () => import("pages/Studentlist.vue"),
        name: "studentlist"
      },
      // Class room Monitor : ห้องเรียนอัจฉริยะ
      {
        path: "classroom",
        component: () => import("pages/CLassroommonitor.vue"),
        name: "classroommonitor"
      },
      // Teaching : การสอน
      {
        path: "teaching",
        component: () => import("pages/Teaching.vue"),
        name: "teaching"
      },

      // Score : คำแนนเก็บ
      {
        path: "score",
        component: () => import("pages/Score.vue"),
        name: "score"
      },
      // Score Setting : การตั้งค่าเกณฑ์คะแนน
      {
        path: "score/setting",
        component: () => import("pages/Scoresetting.vue"),
        name: "scoresetting"
      },
      // Score Report : ผลการเรียน
      {
        path: "scorereport",
        component: () => import("pages/Scorereport.vue"),
        name: "scorereport"
      },

      // Score Report Details : รายละเอียด ผลการเรียน
      {
        path: "scorereport/details/:key/:academicyear/:room/:studentno",
        component: () => import("pages/Scorereportdetails.vue"),
        name: "scoredetails"
      },
      // Page Setup : ตั้งค่าหน้าปริ้น
      {
        path: "pagesetup",
        component: () => import("pages/Pagesetup.vue"),
        name: "pagesetup"
      },
      // Page Setup 2 : ตั้งค่าหน้าปริ้น
      {
        path: "pagesetup2",
        component: () => import("pages/Pagesetup2.vue"),
        name: "pagesetup2"
      },
      // Edit Profile : แก้ไขโปรไพล์
      {
        path: "editprofile",
        component: () => import("pages/Profile.vue"),
        name: "editprofile"
      },
      // score review : ผลคะแนนทบทวน
      {
        path: "scorereview",
        component: () => import("pages/scorereview.vue"),
        name: "scorereview"
      },
      // progress review : ความก้าวหน้าทบทวน
      {
        path: "progressreview",
        component: () => import("pages/progressreview.vue"),
        name: "progressreview"
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
