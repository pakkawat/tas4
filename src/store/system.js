const state = {
  tasData: [], //ข้อมูล user
  tasuserKey: 0, //user key ใช้ check การ login ซ้อน
  tasschoolcode: 0,
  tasacademicyear: "",
  tasclassroom: "",
  taslinkCode: 'mainmenu',
  taslinkmenu: 2,
  taslevelactive: 1,
  tasunitactive: 1,
  taspracticeKey: "",
  tasSettingModeKey: "",
  tasclassActive: "p1", // เก็บค่าวัดระดับ
  tasExam: "",
  tasstudentlist: {},
  tasstudentscore: {},
}

const mutations = {
  setLinkCode: (state, payload) => { //Set ค่า userData
    state.taslinkCode = payload
  },
  setUserKey: (state, payload) => { //Set ค่า userKey
    state.tasuserKey = payload
  },
  setUserData: (state, payload) => { //Set ค่า userData
    state.tasData = payload
  },
  setExamData: (state, payload) => { //Set ค่า tasExam
    state.tasExam = payload
  },
  setLevelData: (state, payload) => { //Set ค่า Level
    state.taslevelactive = payload
  },
  setUnitData: (state, payload) => { //Set ค่า Unit
    state.tasunitactive = payload
  },
  setAcademicData: (state, payload) => { //Set ค่า Academic Year
    state.tasacademicyear = payload
  },
  setClassroomData: (state, payload) => { //Set ค่า Academic Year
    state.tasclassroom = payload
  },
  setStudentScoreData: (state, payLoad) => { //Set เก็บข้อมูลนักเรียนในหน้า : ผลการเรียน
    state.tasstudentscore = payLoad
  },
  setStudentListData: (state, payLoad) => { //Set เก็บข้อมูลนักเรียนในหน้า : ผลการเรียน
    state.tasstudentlist = payLoad
  },
  setLinkMenu: (state, payload) => {
    state.linkmenu = payload
  },

}

const actions = {
  setLinkCode: ({
    commit
  }, payload) => {
    commit('setLinkCode', payload)
  },
  setUserData: ({
    commit
  }, payload) => {
    commit('setUserData', payload)
  },
  setUserKey: ({
    commit
  }, payload) => {
    commit('setUserKey', payload)
  },
  setExamData: ({
    commit
  }, payload) => {
    commit('setExamData', payload)
  },
  setLevelData: ({
    commit
  }, payload) => {
    commit('setLevelData', payload)
  },
  setUnitData: ({
    commit
  }, payload) => {
    commit('setUnitData', payload)
  },
  setAcademicData: ({
    commit
  }, payload) => {
    commit('setAcademicData', payload)
  },
  setClassroomData: ({
    commit
  }, payload) => {
    commit('setClassroomData', payload)
  },
  setStudentScoreData: ({
    commit
  }, payload) => {
    commit('setStudentScoreData', payload)
  },
  setStudentListData: ({
    commit
  }, payload) => {
    commit('setStudentListData', payload)
  },
  setLinkMenu: ({
    commit
  }, payload) => {
    commit('setLinkMenu', payload)
  },
}

export default {
  state,
  mutations,
  actions
}
